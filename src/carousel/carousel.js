import React from "react";
import { CarouselInnerWrapper, CarouselOuterWrapper, Button } from "./style";
import { Left, Right } from "./assets";

class Carousel extends React.Component {
  constructor(props) {
    super(props);
    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.handleSnap = this.handleSnap.bind(this);
    this.onMouseClicked = this.onMouseClicked.bind(this);
    this.onTouchMove = this.onTouchMove.bind(this);
    this.onTouchStart = this.onTouchStart.bind(this);

    this.state = {
      currentItem: 0,
      isExtend: false,
      clicked: false,
      isDown: false,
      startX: null,
      x: 0,
    };
  }

  ref = React.createRef();

  componentDidMount() {
    this.ref.current.addEventListener("transitionend", this.onTransitionEnd);
  }

  componentWillUnmount() {
    this.ref.current.removeEventListener("transitioned", this.onTransitionEnd);
  }

  animating = false;

  componentDidUpdate(prevProps, prevState) {
    const { currentItem, isExtend } = this.state;
    const { currentItem: oldCursor } = prevState;

    if (currentItem !== oldCursor) {
      this.animating = true;
    }

    if (isExtend) {
      setTimeout(() => {
        this.animating = false;
        this.setState({
          isExtend: false,
        });
      }, 1);
    }
  }

  onTransitionEnd = () => {
    const { currentItem } = this.state;
    const { children } = this.props;
    const length = React.Children.count(children);

    this.animating = false;

    if (currentItem >= length) {
      this.setState({
        isExtend: true,
        currentItem: 0,
      });

      return;
    }

    if (currentItem <= -1) {
      this.setState({
        isExtend: true,
        currentItem: length - 1,
      });

      return;
    }
  };

  toSlideHandler = (displacmentToMove) => {
    const { currentItem } = this.state;

    if (this.animating) {
      return;
    }

    this.setState({
      currentItem: currentItem + displacmentToMove,
    });
  };

  renderChildren() {
    const { children: childrenElements } = this.props;
    let children = React.Children.toArray(childrenElements);
    children = [].concat(children, children, children);

    return children.map((child, index) => {
      return React.cloneElement(child, { key: index });
    });
  }

  onMouseDown(e) {
    e.preventDefault();
    const slide = this.ref.current;
    const _startX = e.pageX - slide.offsetLeft;
    const _x = e.pageX - slide.offsetLeft;
    this.setState({
      isDown: true,
      startX: _startX,
      x: _x,
    });
  }

  onMouseClicked(e) {
    e.preventDefault();
    this.setState({
      clicked: true,
    });
  }

  onMouseMove(e) {
    e.preventDefault();
    const { isDown, clicked } = this.state;
    const slide = this.ref.current;
    if (!isDown || !clicked) return;
    const x = e.pageX - slide.offsetLeft;
    this.setState({
      x: x,
    });
  }

  handleSnap(e) {
    e.preventDefault();
    const { x, startX, clicked } = this.state;
    if (!clicked) return;
    this.setState({
      isDown: false,
      clicked: false,
    });
    if (startX < x) {
      this.toSlideHandler(-1);
    } else if (startX > x) {
      this.toSlideHandler(1);
    }
  }

  onTouchStart(e) {
    const slide = this.ref.current;
    const touch = e.changedTouches[0];
    const _startX = touch.clientX - slide.offsetLeft;
    const _x = touch.clientX - slide.offsetLeft;
    this.setState({
      isDown: true,
      startX: _startX,
      x: _x,
      clicked: true,
    });
  }
  onTouchMove(e) {
    if (e.changedTouches && e.changedTouches.length) {
      const { isDown, clicked } = this.state;
      if (!isDown || !clicked) return;
      const slide = this.ref.current;
      const touch = e.changedTouches[0];
      const x = touch.clientX - slide.offsetLeft;
      this.setState({ x: x });
    }
  }

  render() {
    const { currentItem, isExtend } = this.state;
    const { children } = this.props;
    const length = React.Children.count(children);

    const style = {
      display: "flex",
      transition: isExtend ? "none" : "all 200ms ease",
      transform: `translateX(-${(length + currentItem) * 250}px)`,
    };

    return (
      <CarouselOuterWrapper>
        <Button onClick={() => this.toSlideHandler(-1)} src={Left} />
        <CarouselInnerWrapper>
          <div
            style={style}
            ref={this.ref}
            onMouseDown={this.onMouseDown}
            onMouseMove={this.onMouseMove}
            onMouseLeave={this.handleSnap}
            onMouseUp={this.handleSnap}
            onClick={this.onMouseClicked}
            onTouchMove={this.onTouchMove}
            onTouchStart={this.onTouchStart}
            onTouchCancel={this.handleSnap}
            onTouchEnd={this.handleSnap}
          >
            {this.renderChildren()}
          </div>
        </CarouselInnerWrapper>
        <Button onClick={() => this.toSlideHandler(1)} src={Right} />
      </CarouselOuterWrapper>
    );
  }
}

export default Carousel;
