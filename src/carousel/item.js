import { ItemContainer, ItemWrapper } from "./style";
import React from "react";

const Item = ({ children }) => {
  return (
    <ItemContainer>
      <ItemWrapper>
        <p unselectable="on">{children}</p>
      </ItemWrapper>
    </ItemContainer>
  );
};

export default Item;
