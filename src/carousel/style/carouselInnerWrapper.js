import styled from "styled-components";

const InnerWrapper = styled.div`
  display: flex;
  width: 800px;
  overflow: hidden;
  padding: 0 5px;
`;
export default InnerWrapper;
