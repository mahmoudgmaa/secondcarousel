import styled from "styled-components";

const ItemWrapper = styled.div`
  width: 240px;
  height: 80px;
  background-color: #111;
  color: white;
  padding: 5px;
  font-size: 4rem;
  display: flex;
  align-items: center;
  justify-content: center;
`;
export default ItemWrapper;
