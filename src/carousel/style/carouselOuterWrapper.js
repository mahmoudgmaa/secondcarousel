import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  background-color: rgba(242, 242, 242, 1);
  padding: 20px 0;
`;
export default Wrapper;
