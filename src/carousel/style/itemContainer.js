import styled from "styled-components";

const ItemContainer = styled.div`
  padding: 10px 5px;
  width: 250px;
  height: 100px;
`;
export default ItemContainer;
