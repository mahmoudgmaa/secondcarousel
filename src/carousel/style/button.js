import styled from "styled-components";
const Button = styled.img`
  width: 4rem;
  height: 4rem;
  color: rgba(17, 17, 17, 0.4);
  background-color: transparent;
  padding: 0 10px;
  border: 0;
  display: block;
  align-self: stretch;
  align-self: center;
  cursor: pointer;
`;
export default Button;
