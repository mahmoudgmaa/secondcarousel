import React from "react";
import { Carousel, CarouselItem } from "./carousel";

const App = () => {
  return (
    <div className="main">
      <Carousel>
        {[...Array(10)].map((item, index) => {
          return <CarouselItem key={index}>{index + 1}</CarouselItem>;
        })}
      </Carousel>
    </div>
  );
};

export default App;
