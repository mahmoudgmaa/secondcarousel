# ScandiWeb Second Carousel Task
A Carousel made for you 
## [Demo](https://scandiweb-second-carousel-task.netlify.app/)



# Scripts
## Starting Carousel App

Run the app in the development mode.

```bash
npm start
```

Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## Building Carousel App

```bash
npm run build
```

Builds the app for production to the build folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

# Contributing

PLEASE review CONTRIBUTING.markdown prior to requesting a feature, filing a pull request or filing an issue.

# License

ISC license
